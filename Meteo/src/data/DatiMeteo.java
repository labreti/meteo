package data;

import java.io.Serializable;
import java.util.Scanner;

public class DatiMeteo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String data;
	private Number temperatura;
	private Number umidita;
	private Number vento;
	private Number pressione;
	
	/* Costruttore: data una stringa csv costruisce l'oggetto */
	public DatiMeteo (String csv) {
//		System.out.println(csv);
		Scanner s = new Scanner(csv);
		s.useDelimiter(",");
		data=s.next();
		temperatura=s.nextInt();
		umidita=s.nextInt();
		pressione=s.nextInt();
		vento=s.nextInt();
		s.close();
	}
	
	/* Convertitore printer friendly */
	public String toString() {
		String s = String.format("Data: %s%nTemperatura\t%d (C)%nUmidità\t\t%d (%%)%nPressione\t%d (mBar)%nVento\t\t%d (m/sec)%n",
				data,temperatura,umidita,pressione,vento);
		return s; 
	}
}